---
title: Week 4
date: 2018-09-23
---

## What did you do this past week? 

This past week we had presentations by a Google team and Hannah Peeler for introductions to Google Cloud Platform (GCP) and Amazon Web Services (AWS), respectively. These demos were very informative and actually came in handy as a personal project I’m currently working on can be made into a web-app and make perfect use of these services with database integration. Finally, on Friday Dr. Downing went over operators and their comparative nuances when used on different objects and types. 
Outside of class, I spent most of the week working between a research lab and polishing up my resume. I had quite a successful day on Thursday at the career fair, too. I ended up making good use of all four hours there, albeit, exhausting, talkative hours. 

## What's in your way? 

Currently in my way is just the stack of lab reports I have to complete by mid-week. This week just happened to line up a lab for all three classes that require them, but I’m already almost done with one so it’s not too bad. 

## What will you do next week? 

Next week I will be brushing up on my front-end skills in preparation for the projects that go on for the rest of the semester. I will also be studying for interviews and hopefully finish a project I’m working on for research. 

## What was your experience of Project #1 (the problem, the overkill requirements of submission, etc.)? 

The Project #1 problem itself was a good warm-up for the semester. It also made use of HackerRank and git, so definitely puts everyone into productive gears looking ahead. The overkill requirements weren’t actually bad themselves, rather the instructions and listings that described them were frustrating. Sometimes they were a little ambiguous or inconsistent, or both. It was stressful trying to make a proper checklist of everything I’ve done and still needed to do, especially with the project details being listed in three different places (the piazza post, the project page spec and rubric, and the workflow page), all having differences. 

## What's your pick-of-the-week or tip-of-the-week? 

My tip-of-the-week is to start studying now for all upcoming exams. Most first round of exams will be taking place within one or two weeks from now so get ahead so you can be comfortable with the material! 
