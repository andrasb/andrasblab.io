---
title: Week 6
date: 2018-10-07
---

## What did you do this past week? 

This past week we really grilled on containers that are iterables, how iterating works under the hood of Python, and building your own iterator. We also wrapped up phase 1 of our semester-long project which was more stressful than ideal. There were two HackerRank in-class activities that were fun and simple, though. 

## What's in your way? 

Personally, I feel like I haven’t been able to retain all of the nuances we’ve explored in Python during lectures, and as Dr. Downing mentioned this past week, it is highly recommended to review and practice the code we run in class again at home. I will definitely be doing more of that from now on. 

## What will you do next week? 

Next week I will be spending a lot of time doing what I mentioned above in preparation for the exam. I will also be learning React so that I can help split more of the project workload with one of my group-mates for the rest of the semester. 

## What was your experience of Project #2? 

Given that we had just a day over a week for this phase, it felt very rushed. Our group was also not able to meet until the Thursday before the deadline to land on a good subject for our project and organize responsibilities. I worked on setting up our AWS console and helped with the front-end of our website, but AWS was definitely the more confusing part. I feel like if the GCP / AWS presentations were more like an example demo rather than a pitch it would have saved us from spending a whole weekend reading AWS documentation. That being said, once I figured it out and went to Hannah’s lab hours it became pretty straight-forward. Thankfully, this next phase is almost a month long, regardless if it’s a substantially larger amount of work. 

## What's your pick-of-the-week or tip-of-the-week? 

Use this website (http://www.vault.com/) to gain some insight to companies you want to apply to. The 3rd floor GDC lab also has a wall of papers for a lot of companies and the benefits they offer. Lastly, the subreddit [r/cscareerquestions/](https://www.reddit.com/r/cscareerquestions/) and the [CS interview questions subreddit](https://www.reddit.com/r/CS_Questions/) sometimes have pretty helpful posts for anyone looking to go into industry soon. 
