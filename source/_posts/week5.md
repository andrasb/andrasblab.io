---
title: Week 5
date: 2018-09-30
---

## What did you do this past week? 

This past week we went over iteration and the reduce function in Python. Iterators were definitely a review for me from Java, but it was still nice exploring the nuances behind Python’s built-in functions and their efficiency - especially when we went over the basic factorial implementations. 
Outside of class, I was able to finish two lab reports and I am getting very close to finishing a personal project I am coding for a research lab. That’s pretty exciting because I will be automating a large pipeline. 

## What's in your way? 

Launching our project on AWS hasn’t seemed so straight-forward, and there have definitely been some snags. For example, reserving a domain on Name-Cheap doesn’t allow you to transfer it until sixty days later… so now I have to pay for a domain on AWS. If anyone knows a workaround for this please let me know. 
It is also getting harder and harder every week to schedule new interviews. There isn’t enough time in a day and there are a few where I have to miss class lectures and find a way to not fall behind. 

## What will you do next week? 

I have two exams on Monday and Tuesday, so that’ll be “fun”. I’ll also be helping to launch our group’s project on AWS by Tuesday night. The rest of the week I’ll try to relax as much as possible while also preparing for another round of interviews. 

## What's your experience of the readings, so far? And, if you went, what did you think of the talk by Under Armour? 

I think the readings are a good part of the curriculum. The first few weeks were a little redundant, but only personally because I’ve read Extreme Programming Installed, Pair Programming, and Collegiate Happiness before. These papers, blogs, and books are definitely recommendable to anyone that is preparing to enter the computer science industry. I’ve learned a lot from the rest of the readings, too, however, I am dreading a lot of these web-based programming languages. 
The talk by Under Armour was pretty good. I enjoyed the discussions regarding the communication and team aspects of working in the industry over the technical presentation (mainly because I’ve taken a mobile app development class already and have experience with designing for the Apple Watch). The questions by other students at the end were the best part as they brought out very insightful responses. 

## What's your pick-of-the-week or tip-of-the-week? 

My tip-of-the-week is to use these two guides for any team choosing to work with AWS and is [new to the setup](https://docs.aws.amazon.com/AmazonS3/latest/dev/website-hosting-custom-domain-walkthrough.html). Then pick a [simple Bootstrap framework](https://getbootstrap.com/docs/4.0/examples/) for your website stylings. 
