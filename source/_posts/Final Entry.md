---
title: Final Entry
date: 2018-12-11
---

## What did you like the least about the class?

Probably the disconnect between the projects and the lecture/course material. It gets pretty jarring up until about Phase Two or Three when the foundation for our website is finally done. While the material I learned from both aspects of the course were helpful (the Python lectures very much so), it got pretty stressful. My advice would be to get Project 1 and 2 done as early as possible, and then focus on nothing but Exam 1, and then focus solely on finishing the rest of the IDB phases and save studying for Exam 2 until after presentations. That way it doesn’t feel as stacked or stressful and seems more like just one course. 

## What did you like the most about the class?

The deep dives and insights on Python code. We went “under-the-hood” countless times and those were the lectures were my mind felt most malleable. 

## What's the most significant thing you learned?

React (I love how much this streamlines web-dev) and experience with working in a development team (splitting roles and handing off between phases). 

## How many hours a week did you spend coding/debugging/testing for this class?

Probably around an average of 6 hours. 8 during the more demanding phases and 4 for the first project and first phase. 

## How many hours a week did you spend reading/studying for this class?

For each lecture, I spent about 45 minutes reviewing any code we explored to fully understand what the point was and what it is actually doing. The formatting with the assertions helped with that, too, and was neat. The required readings usually took almost an hour unless they were articles or blogs which took maybe thirty minutes. In total, approximately three hours. 

## How many lines of code do you think you wrote?

Several hundred but I think less than a thousand. I bounced between back-end and front-end very often since our fifth member was different for almost every phase, but in the end it was mostly front-end. This also involved constantly changing single lines or methods at a time to see how it would look in real-time. 

## What required tool did you not know and now find very useful?

Easy -- AWS (or GCP I’m sure, too, we just chose the other). Having been the one to set it up for the first phase and reading a lot of documentation to know everything it can do I learned that it is awesome. It really helps that it’s free for students, too. A word of advice is to just register your domain on AWS instead of what was recommended by the course website (namecheap and others). It’s complicated and can cost more money to transfer registrars and domain hosts so just try to do it with whichever you plan to work with. 

## What's the most useful Web dev tool that your group used that was not required?

We actually didn’t use anything that wasn’t already required, so I think that’s a good thing considering how the instructions/workflow were sometimes a bit vague and open-ended. I know other groups substituted certain frameworks because they had past experience or knowledge so definitely ask Professor Downing if you can use something that maybe you’re already familiar with. Most of those groups said it was easier that way. Otherwise, React was the most useful. 

## If you could change one thing about the course, what would it be?

The disconnect between the projects and the rest of the course. I really liked the lecture material so I wouldn’t want to get rid of or replace a lot of it, but maybe have two days of lecture per phase (or one for the phases that are only two or one week(s) long) where Professor Downing or a TA can walk-through and hit the main points of the project during that phase. Or maybe have a lab session dedicated to teaching examples of web-dev implementations. 