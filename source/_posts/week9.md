---
title: Week 9
date: 2018-10-28
---

## What did you do this past week?

This past week we spent a lot of time going over relational algebra and implementing different join functions. As someone that is new to database programming and SQL, these Python implementations of operations were nice to explore because they made it very clear how they work and what their effective use cases are. Wednesday was a special lecture, as we had a presentation led Dr. Elaine Rich and Dr. Alan Cline on ethics in the software engineering world. I will write more on that lecture below. 

## What's in your way?

I have been working on a pretty long research paper, so that on top of working with the group on project three for the Tuesday deadline is what is in my way right now. 

## What will you do next week?

Next week I assume we will review more of what it means to program and work with databases more specifically. Dr. Alan Cline and Dr. Elaine Rich also plan to have a follow-up lecture on ethics in the programming world. Of course, there is also the plan of submitting Phase Two of the project for this class and my research paper. Something that is a bit more exciting, is that on Monday I am registering for courses for what I plan to be my last semester as an undergraduate! 

## What did you think of the talk by Dr. Rich and Dr. Cline?

I loved it. I thoroughly enjoy these types of talks regarding ethics and how these types of decisions are made, mostly because I think they are important to our society to explore them and try to make the best decision. When it comes to any controversial issue, we need as many people possible to provide their relevant input in order to gain as much knowledge about the issue and the impacts each decision could have. Each decision can have consequences that affect people’s lives whether it is directly or indirectly, and another huge portion of studying ethics is knowing how to actually handle how you act on the decision you make. 

## If you went, what did you think of the talk by Atlassian?

I thought it was great to have someone that has directly experienced a product acquisition share some of their feelings of that event and some of their insight into the industry. There was a pretty clear explanation of how acquisitions could occur in different ways and each one’s level of job security when it comes to being an employee. I was looking forward to their public mock interview, especially since they mentioned it so many times and wanted the talk to be different than so many other company talks, so that’s a shame. 

## What's your pick-of-the-week or tip-of-the-week?

In light of the Trello acquisition by Atlassian and talks of the Red Hat acquisition by IBM, a good practice to anyone who is entering the software industry or in the process of searching for openings is probably to include job security and queries based on acquisitions in your research on a company. 
