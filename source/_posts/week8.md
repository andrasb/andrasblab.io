---
title: Week 8
date: 2018-10-21
---

## What did you do this past week? 

This past week was pretty calm. Of course, there was Exam 1 on Monday, but the rest of the week felt nice to get back into the normal groove of things since exams are over. On Wednesday, we took time in lecture to meet with our customer and developer teams and review each other’s progress and mission for the rest of the semester-long project. It was quite reassuring to see most of us were on the same page. Outside of this class, I had to finish a long research proposal for Wednesday, as well as finish some lab reports. On Friday we went over more Python functions (select and project) and their implementations. I’ve been enjoying these in-class HackerRanks and I wish more classes in UTCS took some time to do similar things. 

## What's in your way? 

Now that I’ve been able to get ahead in classwork for all of my courses again, the next stressful thing on the to-do list is to continue working on Phase 2 of our interactive database. 

## What will you do next week? 

Most of my time next week will be dedicated to Phase 2 of our IDB, along with adding the finishing touches to one of my personal projects. I will also be putting in some serious work hours in the biochemistry research lab I am a part of, so that will be fun! 

## What was your experience of Test #1 (the problems, the time, HackerRank)? 

I thought the problems were perfectly appropriate for the material we have covered and were pretty straightforward. More time would always be appreciated for some relief, but 90 minutes isn’t too short. My anxiety got to me when it came to the iterator question, as instead of doing the simple solution I basically began writing a full implementation of the arithmetic. As for HackerRank, I thought it was completely normal. I was curious to see what the multiple-choice, quiz-like questions would look like on that kind of setup, but I’m actually a bit glad none showed up. 

## What's your pick-of-the-week or tip-of-the-week? 

My tip of the week is to plan out your registration now or as soon as possible. The course schedule for Spring 2019 is up now and registration will start soon so make sure you have a good idea of which classes you need to take, which classes you can take, and how well you can fit them into a schedule! Also, make sure to check often, maybe daily, as some courses can be canceled and new ones can show up! 
