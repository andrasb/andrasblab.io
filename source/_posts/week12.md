---
title: Week 12
date: 2018-11-18
---

## What did you do this past week?

This past week we reviewed even more SQL examples. I felt that Monday and Wednesday definitely solidified everyone’s understanding of it, especially as we spent almost the entire lecture taking turns explaining an example as part of attendance/participation. On Wednesday we started material about refactoring code, which is one of my favorite aspects of software development and engineering. Phase Three of our project was due this past Tuesday, too. More on that later. 

## What's in your way?

Currently in my way is just a bunch of coursework that is unrelated to this class. I have a revised research paper due at the end of this upcoming week, as well as a couple of lab reports due just before the Thanksgiving break. Besides that, it will be adding the finishing touches to our IDB website. Also, I plan on reviewing parts of the project that I didn’t work on so as to familiarize myself with those portions of code. Hopefully I will also have the chance to explain parts that I did work on to the rest of my group. 

## What will you do next week?

Next week we will be doing more refactoring examples and then taking Thanksgiving break! Happy early turkey day everyone! Before the festivities I will have to complete a few quizzes for other classes, as well as research paper and lab report I mentioned above. Towards the end of the week I will have to work on a problem set, and then it’s just polishing what we need to submit for the final project phase. 

## What was your experience of Project #4?

Project #4 (or Phase Three of our interactive database project) has been my favorite so far, just because our group finally got to see the fruits of our labour come together to what customers can recognize as an actual, functioning product. 

## What's your pick-of-the-week or tip-of-the-week?

My tip of the week is to read into some quantum computing if you haven’t already. Whether or not you want to pursue any career or education specifically in that field, it is an incredible technology whose mechanics are very interesting. There is also a lot of transferable knowledge in learning some of the basics of quantum physics and quantum algorithms towards the rest of the computer science world. 